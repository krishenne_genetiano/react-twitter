###Getting Started###

Install NodeJS
```
https://nodejs.org/en/download/
```

Install Git
```
https://git-scm.com/downloads
```

Checkout this repo, install dependencies, then start the app with the following:

```
	> git clone https://barrylavides@bitbucket.org/barrylavides/react-twitter.git
	> cd react-twitter
	> npm install
	> npm start
```
Open http://localhost:8080/ in your browser
